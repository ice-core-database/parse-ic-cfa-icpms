parse_iso_csv <- function(iso_path, clean) {
  if (!(is.data.frame(clean))) { warning('clean is not a data.frame') }
  if (!is.character(iso_path) || !file.exists(iso_path)) { warning('iso has an incorrect path') }

  iso <- read.csv(iso_path)

  iso$Depth.Avg <- round(iso$Depth.Avg, digits = 2)

  #Fixing the ISO timescale
  #Top Time
  depth2t.top <- approxfun(x = clean$Top.depth..m., y = clean$Top.Time..11.8.17.Timescale., rule = 2:1)

  iso$Top.Time.11.8.17 <- depth2t.top(iso$Depth..Top.)

  #Bottom Time
  depth2t.bot <- approxfun(x = clean$Bottom.Depth..m., y = clean$Bot.Time..11.8.17.timescale., rule = 2:1)

  iso$Bot.Time.11.8.17 <- depth2t.bot(iso$Depth..Bot.)

  #Average Time
  depth2t.avg <- approxfun(x = clean$Middle.Depth..m., y = clean$Avg.Time..11.8.17.timescale., rule = 2:1)

  iso$Avg.Time.11.8.17 <- depth2t.avg(iso$Depth.Avg)

  #Average Age
  depth2a.top <- approxfun(x = clean$Top.depth..m., y = clean$Top.Age..yrs., rule = 2:1)

  iso$Top.Age <- depth2a.top(iso$Depth..Top.)

  #Bottom Age
  depth2a.bot <- approxfun(x = clean$Bottom.Depth..m., y = clean$Bot.Age..yrs., rule = 2:1)

  iso$Bot.Age <- depth2a.bot(iso$Depth..Bot.)

  #Average Age
  depth2a.avg <- approxfun(x = clean$Middle.Depth..m., y = clean$Avg.Age..yrs., rule = 2:1)

  iso$Avg.Age <- depth2a.avg(iso$Depth.Avg)

  return(iso)
}