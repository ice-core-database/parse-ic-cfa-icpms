parse_tubes_csv <- function(tubes_path, start_tube, sample_core_id) {
  tubes <- read.csv(tubes_path)

  colnames(tubes) <- c('tube_name', 'depth_top', 'depth_bottom', 'length', 'core_diameter_cm', 'lengthfordensity', 'mass',
                       'density', 'notes', 'top_we')
  tubes <- tubes %>% select(c('tube_name', 'depth_top', 'depth_bottom', 'length', 'core_diameter_cm', 'density', 'top_we'))

  tubes$tube_id <- start_tube:(start_tube - 1 + nrow(tubes))
  tubes$core_id <- sample_core_id
  return(tubes)
}

## function to match tubes FROM CSV to rows of data
## TODO consider vectorizing
get_tube_id <- function(depth, tubes) {
  tmp_tubes <- tubes %>%
    drop_na("depth_top") %>%
    drop_na("depth_bottom") %>%
    select("tube_id", "depth_top", "depth_bottom")
  res_index <- detect_index(tmp_tubes[tmp_tubes$depth_top <= depth,]$depth_bottom, ~.x > depth)
  if (res_index == 0) {
    warning("No match found for depth")
    return(NaN)
  }
  return(tmp_tubes$tube_id[res_index])
}