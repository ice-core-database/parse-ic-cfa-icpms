#requiRements::install(path_to_requirements = 'requirements.r.txt')

library(tidyverse)
library(DBI)
library(naniar)
library(progress)
library(dotenv)
library(dplyr)
library(purrr)
#load_dot_env(file = ".env")
load_dot_env(file = "dev.env")

source('metadata_dictionaries.R')
source('parse.R')
source('db_utils.R')

## PLEASE REVIEW AND CHANGE THE FOLLOWING SECTION FOR EACH INDIVIDUAL UPLOAD!! ##

# softcoded metadata for this set of samples:
sample_core_id <- core_id["DEN-13B"]

cfa_lab_id <- lab_id["ICE Lab"]
cfa_instrument_id1 <- instrument_id["Abakus laser particle detector"]
cfa_instrument_id2 <- instrument_id["conductivity meter"]

ic_lab_id <- lab_id["ICE Lab"]
ic_instrumet_id <- instrument_id["ICS5000 capillary ion chromatograph"]
ic_units <- 'ppt'

iso_lab_id <- lab_id["Stable Isotope Lab"]
iso_instrument_id <- instrument_id["L2130i Cavity Ring Down Spectrometer"]

icpms_lab_id <- lab_id["CCI ICP-MS Lab"]
icpms_instrument_id <- instrument_id["ELEMENT 2 high-resolution sector field ICP-MS"]

cfa_path <- '~/Denali/R/unevenly-spaced-timeseries/Timescale Correction/DIC2_CFA.csv'
ic_icpms_clean_path <- '~/Denali/R/unevenly-spaced-timeseries/Timescale Correction/DIC2_clean.csv'
iso_path <- '~/Denali/R/unevenly-spaced-timeseries/Timescale Correction/DIC2_HighRes_ISO.csv'
tubes_path <- '~/Denali/North_Pacific_Data/Denali/DIC2/DIC2_Properties.csv'


## DATABASE CONNECTION
con <- dbConnect(RPostgres::Postgres(),
                 dbname = Sys.getenv("DATABASE_NAME"),
                 host = Sys.getenv("DATABASE_HOST"),
                 port = Sys.getenv("DATABASE_PORT"),
                 user = Sys.getenv("DATABASE_USER"),
                 password = Sys.getenv("DATABASE_PASS"))

## Generate and Upload Tubes
tubes <- parse_tubes_csv(tubes_path,
                         start_tube = get_first_tubeID(con),
                         sample_core_id = as.integer(sample_core_id))
dbAppendTable(con, Id(schema = "denali", table = "tube"), tubes)

## DATA PARSING
ic_icpms_clean <- parse_ic_icpms_csv(ic_icpms_clean_path)
iso <- parse_iso_csv(iso_path, ic_icpms_clean)
jointDataset <- merge_by_timescale(ic_icpms_clean = ic_icpms_clean,
                                   iso = iso,
                                   sample_core_id = as.integer(sample_core_id))

# Joint Dataset
## Generate Samples and Match Tubes in the Joint Dataset
sample_data <- jointDataset %>% get_sample_table()
dbAppendTable(con, Id(schema = "denali", table = "sample"), sample_data)

## upload ISO
iso_data <- jointDataset %>% get_iso_table(con,
                                           lab_id = iso_lab_id,
                                           instrument_id = iso_instrument_id)
dbAppendTable(con, Id(schema = "denali", table = "iso"), iso_data)

## Upload ICPMS
icpms_data <- jointDataset %>% get_icpms_table(con,
                                               lab_id = icpms_lab_id,
                                               instrument_id = icpms_instrument_id)
dbAppendTable(con, Id(schema = "denali", table = "icpms"), icpms_data)

## Upload IC
ic_data <- jointDataset %>% get_ic_table(con,
                                         lab_id = ic_lab_id,
                                         instrument_id = ic_instrumet_id,
                                         ic_units = ic_units)
dbAppendTable(con, Id(schema = "denali", table = "ic"), ic_data)

## Process CFA
cfa <- parse_cfa_csv(cfa_path,
                     ic_icpms_clean,
                     tubes,
                     con,
                     core_id = sample_core_id,
                     lab_id = cfa_lab_id,
                     instrument_id1 = cfa_instrument_id1,
                     instrument_id2 = cfa_instrument_id2)

# Build CFA samples
samples.cfa <- cfa %>% get_samples_table_cfa()
dbAppendTable(con, Id(schema = "denali", table = "sample"), samples.cfa)

## make cfa_data for upload
cfa_data <- cfa %>% get_cfa_table(con)
dbAppendTable(con, Id(schema = "denali", table = "cfa"), cfa_data)

## disconnect from database
dbDisconnect(con)